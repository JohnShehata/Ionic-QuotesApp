import { QuotesService } from './../../services/quotes';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Quote } from '../../data/quote.interface';

/**
 * Generated class for the QuotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage implements OnInit {

  quote:{category:string,quotes:Quote[],icon:string}[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private AlertCntrl:AlertController,private QuotesService:QuotesService) {
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad QuotesPage');
  }

  ngOnInit()
  {
    this.quote=this.navParams.data;
  }

  AddToFavorites(SelectedQuote:Quote)
  {

    let confirm = this.AlertCntrl.create({
      title: 'Use this lightsaber?',
      message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Yes ,please ',
          role:'cancel',
          handler: () => {
            this.QuotesService.AddFavorites(SelectedQuote)
          }
        },
        {
          text: 'No ,thank you',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  
  }

  IsFavorite(SelectedQuote:Quote)
  {
      return this.QuotesService.IsFavoriteQuote(SelectedQuote);
  }
  RemoveFromFavorite(SelectedQuote:Quote)
  {
      this.QuotesService.RemoveFavorite(SelectedQuote);
  }
}
