import { Quote } from './../../data/quote.interface';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the QuotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quote',
  templateUrl: 'quote.html',
})
export class QuotePage implements OnInit {

  Author:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ViewCntrl:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuotePage');
  }

  ngOnInit()
  {
    this.Author=this.navParams.get('person');
  }
  OnClose(remove:boolean)
  {
      this.ViewCntrl.dismiss(remove);
  }
}
