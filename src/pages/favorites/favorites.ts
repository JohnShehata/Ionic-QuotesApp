import { SettingsService } from './../../services/settings';
import { QuotePage } from './../quote/quote';
import { Quote } from './../../data/quote.interface';
import { QuotesService } from './../../services/quotes';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage implements OnInit {

  FavoriteQuotes:Quote[];
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private QuotesService:QuotesService,
    private ModalCntrl:ModalController,
  private SettingsService:SettingsService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }
  ngOnInit()
  {
   
  }

  ionViewWillEnter()
  {
    this.FavoriteQuotes=this.QuotesService.GetFavoriteQuotes();
  }

  OnViewQuote(SelectedQuote:Quote)
  {
      const Modal=this.ModalCntrl.create(QuotePage,SelectedQuote);
      Modal.present();
      Modal.onDidDismiss((remove:boolean)=>{
        if(remove)
        {
          this.QuotesService.RemoveFavorite(SelectedQuote);
          this.FavoriteQuotes=this.QuotesService.GetFavoriteQuotes();
        }
      });
  }
  OnRemoveFromFavorite(SelectedQuote:Quote)
  {
      this.QuotesService.RemoveFavorite(SelectedQuote);
      this.FavoriteQuotes=this.QuotesService.GetFavoriteQuotes();
  }

  GetBackground()
  {
      return this.SettingsService.IsAlternateBackground?'quotebackground':'altBackground';
  }
}
