import { SettingsPage } from './../pages/settings/settings';
import { TabsPage } from './../pages/tabs/tabs';
import { FavoritesPage } from './../pages/favorites/favorites';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  tabsPage=TabsPage;
  settingsPage=SettingsPage;
  @ViewChild('nav') nav:NavController
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private MenuCntrl:MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(Page:any)
  {
      this.nav.setRoot(Page);
      this.MenuCntrl.close();
  }
}

