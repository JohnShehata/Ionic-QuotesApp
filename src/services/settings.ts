
export class SettingsService
{
    private _IsAlternateBackground:boolean;

   
    get IsAlternateBackground():boolean
    {
        return this._IsAlternateBackground;
    }

    set IsAlternateBackground(IsAlt:boolean)
    {
        this._IsAlternateBackground=IsAlt;
    }
}