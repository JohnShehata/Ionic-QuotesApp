import { Quote } from './../data/quote.interface';

export class QuotesService
{
    private FavoritesQuotes:Quote[]=[];
    AddFavorites(FQuote:Quote)
    {
        this.FavoritesQuotes.push(FQuote);
    }

    RemoveFavorite(FQuote:Quote)
    {
      const Index= this.FavoritesQuotes.findIndex((q)=>{
                return FQuote.id==q.id;
        })
        this.FavoritesQuotes.splice(Index,1);
    }

    GetFavoriteQuotes()
    {
        return this.FavoritesQuotes.slice();
    }

    IsFavoriteQuote(SelctedQuote:Quote)
    {
       return this.FavoritesQuotes.find((quoteElem)=>{
            return quoteElem.id==SelctedQuote.id;
       });     
    }
}